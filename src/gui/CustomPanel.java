package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class CustomPanel extends JPanel implements MouseListener
{
    boolean isHighlighted;
    public static int highlightCount;
    public static  int slideCount;
    public int id;
    public static int selected[] = new int[2];
    Border blackBorder = BorderFactory.createLineBorder(Color.BLACK);
    Border blueBorder = BorderFactory.createLineBorder(Color.BLUE,5);
    
    public void init() {
    	
    }
    
    
    CustomPanel()
    {
 
    	id = slideCount;
    	++slideCount;
    	addMouseListener(this);
        setBorder(blackBorder);
        setFocusable(true);
    }
//
//    @Override
//    public Dimension getPreferredSize()
//    {
//        return new Dimension(200, 100);
//    }

    @Override public void mouseClicked(MouseEvent e)
    {
        if(isHighlighted)
        {
        	setBorder(blackBorder);
        	if (highlightCount > 0){
        		--highlightCount;
        		isHighlighted=!isHighlighted;
        	}
        }
        else if (!isHighlighted && highlightCount < 2) {
        	setBorder(blueBorder);
        	selected[highlightCount]=id;
        	++highlightCount;
            isHighlighted=!isHighlighted;
        }
    }

    @Override public void mousePressed(MouseEvent e){}
    @Override public void mouseReleased(MouseEvent e){}
    @Override public void mouseEntered(MouseEvent e){}
    @Override public void mouseExited(MouseEvent e){}
}
